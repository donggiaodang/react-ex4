import { ADD_TO_CART, INCREASE_DECREASE_NUM, MORE_DETAIL } from "../constants/shoeConstant"

export const handleChangeDetailShoe=(value)=>{
    return {
        type: MORE_DETAIL,
        payload: value,
        
    }
}
export const addToCart = (value)=>{
    return{
        type: ADD_TO_CART,
        payload: value,
    }
}

export const increaseDecreaseNum = (idvalue,quantity)=>{
    return{
        type: INCREASE_DECREASE_NUM,
        payload:{
            id: idvalue,
            quantity: quantity
        }
    }
}