import { dataShoe } from "../dataShoe";
import { ADD_TO_CART, INCREASE_DECREASE_NUM, MORE_DETAIL } from "./constants/shoeConstant";

let innitialState = {
    shoeArr: dataShoe,
    detail: dataShoe[0],
    cart: [],
}

export const shoeReducer = (state = innitialState, action)=>{
    // action stand for the entire object in shoeActions file
    switch(action.type){
        case MORE_DETAIL:{
            return {...state,detail:action.payload}
        }
        case ADD_TO_CART:{
            let cloneCart = [...state.cart]
            let index = cloneCart.findIndex((item)=>{
                return item.id == action.payload.id
            })
            if(index ==-1){
                let cartItem = {...action.payload,number:1}
                cloneCart.push(cartItem)
            }else{
                cloneCart[index].number++
            }
            return {...state,cart:cloneCart}
        }
        case INCREASE_DECREASE_NUM:{
            let cloneCart = [...state.cart]
            let index = cloneCart.findIndex((item)=>{
                return item.id == action.payload.id
            })
            if(index!==-1){
                cloneCart[index].number += action.payload.quantity
                if(cloneCart[index].number==0){
                    cloneCart.splice(index,1)
                }
            }
            return {...state,cart:cloneCart}
        }
        default:
            return state
    }
}