import React, { Component } from "react";
import { connect } from "react-redux";
import { increaseDecreaseNum } from "./redux/actions/shoeAction";
import { INCREASE_DECREASE_NUM } from "./redux/constants/shoeConstant";

class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>
            <img style={{ width: 100 }} src={item.image} alt="" />
          </td>
          <td>{item.name}</td>
          <td>{item.price *item.number} $</td>
          <td>
            <button onClick={()=>{this.props.handleIncreaseDecreaseQuan(item.id,-1)}} className="btn btn-danger">-</button>
            <span className="m-2">{item.number}</span>
            <button onClick={()=>{this.props.handleIncreaseDecreaseQuan(item.id,+1)}} className="btn btn-success">+</button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <th>ID</th>
            <th>Image</th>
            <th>Name</th>
            <th>Price</th>
            <th>Quantity</th>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProp = (state) => {
  return {
    cart: state.shoeReducer.cart,
  };
};

let mapDispatchToProps =(dispatch)=>{
    return {handleIncreaseDecreaseQuan : (id,quantity)=>{
        dispatch(increaseDecreaseNum(id,quantity))
    }
}
}
export default connect(mapStateToProp,mapDispatchToProps)(Cart);
