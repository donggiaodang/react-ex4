import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addToCart, handleChangeDetailShoe } from './redux/actions/shoeAction'
import { ADD_TO_CART, MORE_DETAIL } from './redux/constants/shoeConstant'

 class ItemShoe extends Component {
  
    render() {
        let {image,name,price}=this.props.data
    return (
      <div className='col-3 p-1'>
       <div className="card h-100 text-left" style={{width: '17rem'}}>
        <img className="card-img-top" src={image} alt="Card image cap" />
        <div className="card-body">
        <h6 className='card-title'>{name}</h6>
        <h2 className="card-text">{price}$</h2>
        <button className='btn btn-secondary' onClick={()=>{this.props.handleShowDetail(this.props.data)}}>More Detail</button>
        <button onClick={()=>{this.props.handleAddToCart(this.props.data)}} className='btn btn-success'>Add To Cart</button>
        </div>
        </div>

      </div>
    )
  }
}

let mapDispatchToProps =(dispatch)=>{
    return{
        handleShowDetail:(shoe)=>{
         dispatch(handleChangeDetailShoe(shoe))
        },
        handleAddToCart:(shoe)=>{
            dispatch(addToCart(shoe))
        }
    }
}

export default connect(null,mapDispatchToProps)(ItemShoe)