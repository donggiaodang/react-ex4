import React, { Component } from "react";
import { connect } from "react-redux";

 class DetailShoe extends Component {
  render() {
    let {image,description,id,name,price}= this.props.detail
    return (
      <div className="row mt-5">
        <div className="col-4 h-50">
            <img style={{width:200}} src={image} alt="" />
        </div>
        <div className="col-8 text-left">
          <p>ID: {id}</p>
          <p>Name: {name}</p>
          <p>Price: {price}</p>
          <p>Description: {description}</p>
        </div>
      </div>
    );
  }
}

let mapStateToProps =(state)=>{
    return {
        detail: state.shoeReducer.detail
    }
}
export default connect(mapStateToProps)(DetailShoe)
